asgiref==3.2.3
Django==3.0.4
djangorestframework==3.11.0
psycopg2==2.8.4
pytz==2019.3
short-url==1.2.2
sqlparse==0.3.1
