from django.urls import path, include
from .views import create_delete_link_view, redirect_view

urlpatterns = [
    path('shorten/', create_delete_link_view, name='create-delete-link'),
    path('<slug:shorten_link>/', redirect_view, name='redirect-view')
]
