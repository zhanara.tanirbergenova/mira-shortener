from django.contrib import admin
from .models import LinkShortener


@admin.register(LinkShortener)
class LinkShortenerAdmin(admin.ModelAdmin):
    list_display = ('original_link', 'shorten_link', 'created_at', 'is_active')
    exclude = ('shorten_link', )