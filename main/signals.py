from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import LinkShortener


@receiver(post_save, sender=LinkShortener)
def save_profile(sender, instance, created, **kwargs):
    if instance.shorten_link is None:
        instance.shorten_link = instance.get_shorten_link()
        instance.save()