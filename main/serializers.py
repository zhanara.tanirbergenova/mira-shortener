from rest_framework import serializers
from .models import LinkShortener


class LinkShortenerSerializer(serializers.ModelSerializer):
    class Meta:
        model = LinkShortener
        fields = ['id', 'original_link', 'created_at', 'updated_at', 'shorten_link']
        read_only_fields = ('shorten_link',)
