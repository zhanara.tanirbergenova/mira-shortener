from django.http import HttpResponseRedirect
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from .models import LinkShortener
from .serializers import LinkShortenerSerializer
from .key_errors import SUCCESS_KEY, ERROR_KEY, NOT_FOUND_MESSAGE, SUCCESS_DELETE_MESSAGE


@api_view(['POST', 'DELETE', 'GET'])
def create_delete_link_view(request):
    if request.method == 'DELETE':
        try:
            original_link = request.data['original_link']
            instance = LinkShortener.objects.get(original_link=original_link, is_active=True)
            instance.is_active = False
            instance.save()
            return Response(data={SUCCESS_KEY: SUCCESS_DELETE_MESSAGE}, status=status.HTTP_204_NO_CONTENT)
        except:
            return Response(data={ERROR_KEY: NOT_FOUND_MESSAGE}, status=status.HTTP_404_NOT_FOUND)
    elif request.method == 'POST':
        serializer = LinkShortenerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'GET':
        try:
            original_link = request.GET.get('url', '')
            link_obj = LinkShortener.objects.get(original_link=original_link, is_active=True)
            serializer = LinkShortenerSerializer(link_obj)
            return Response(serializer.data)
        except LinkShortener.DoesNotExist:
            return Response(data={ERROR_KEY: NOT_FOUND_MESSAGE}, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def redirect_view(request, shorten_link):
    try:
        link_obj = LinkShortener.objects.get(shorten_link=shorten_link, is_active=True)
        return HttpResponseRedirect(redirect_to=link_obj.original_link)
    except:
        return Response(data={ERROR_KEY: NOT_FOUND_MESSAGE}, status=status.HTTP_404_NOT_FOUND)


