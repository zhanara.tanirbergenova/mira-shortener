from django.db import models
import short_url


class LinkShortener(models.Model):
    class Meta:
        verbose_name = 'Shorten link'
        verbose_name_plural = 'Shorten links'

    original_link = models.TextField(unique=True)
    shorten_link = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.original_link

    def get_shorten_link(self):
        return short_url.encode_url(self.id)


