SUCCESS_KEY = 'success'
ERROR_KEY = 'error'
NOT_FOUND_MESSAGE = 'Entry not found'
SUCCESS_DELETE_MESSAGE = 'Entry successfully deleted'
